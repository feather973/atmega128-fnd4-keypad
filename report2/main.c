/*
 * newcalc.c
 *
 * Created: 2020-07-30 오전 9:18:12
 * Author : kccistc
 */ 

/* 시계 + button 으로 시간 set */

#define F_CPU	16000000
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

//// fnd4 print logic start
// 중요생각 : 출력인덱스를 배열로
unsigned char digits[] = {0b00100010, 0b11111010, 0b01000110, 0b01010010, 0b10011010,
0b00010011, 0b00000011, 0b00111010, 0b00000010, 0b00011010};

int timer = 0;
int i = 0;
int disp_order[] = {1000, 100, 10, 1};
int count_1s = 1;					// timer2_ovf 에서 카운트
int blink = 0;

enum clockstate {
	DEFAULT,
	SET_THOUSAND,
	SET_HUNDRED,
	SET_TEN,
	SET_ONE
};

enum clockstate stat = DEFAULT;		// timer2_ovf 에서 설정

ISR(TIMER0_OVF_vect){   // bit not(~) needed
	if(stat != DEFAULT){			// 시간세팅중엔 깜빡이게 설정
		if(blink == 0){
			PORTE = 0b11111111;		// off
		}else{
			PORTE = ~(1<<i);	// bit set
			PORTD = ~digits[timer/disp_order[i]%10];
			i = (i==3)? 0 : i+1;
		}
	}else{							// 기본동작
		PORTE = ~(1<<i);	// bit set
		PORTD = ~digits[timer/disp_order[i]%10];
		i = (i==3)? 0 : i+1;
	}
}

void timer0_init(){		// /64
	TCCR0 |= (1 << CS02);
	TIMSK |= (1 << TOIE0);
	sei();
}

ISR(TIMER2_OVF_vect){
	count_1s++;
	
	if(stat == DEFAULT){	
		if(count_1s%64 == 0){	// 1s
//			timer = (timer==9999)? 0 : timer+1;
			if(timer == 5959){
				timer = 0;
			}else if(timer % 1000 == 959){
				timer -= 959;
				timer += 1000;
			}else if(timer % 100 == 59){
				timer -= 59;
				timer += 100;
			}else{
				timer++;
			}
		}
	}else{
		if(count_1s%32 == 0){	// 0.5s
			blink = (blink==1)? 0 : 1;
		}
	}
}

void timer2_init(){		// /1024
	TCCR2 |= (1 << CS02) | (1 << CS00);
	TIMSK |= (1 << TOIE2);
//	sei();
}
//// fnd4 print logic end

//// 4x4 keymat logic start
// 중요생각 : 4bit 씩 비트제어,  키 리턴시 정수값이 아닌, 비트값을 리턴
// tradeoff : getkey 로직을 단순하게  vs  getkey 리턴을 정수로
#define KEY0		0b10000100
#define KEY1		0b01001000
#define KEY2		0b01000100
#define KEY3		0b01000010
#define KEY4		0b00101000
#define KEY5		0b00100100
#define KEY6		0b00100010
#define KEY7		0b00011000
#define KEY8		0b00010100
#define KEY9		0b00010010
#define PLUS		0b00010001
#define MINUS		0b00100001
#define MULTIPLY	0b01000001
#define DIVIDE		0b10000001
#define CLEAR		0b10000010
#define SPECIAL		0b10001000

/*	PORTA [ row input 4bit ] [ column output 4 bit ]
// input internal pull-up --> cathode input need
	PA  3  2  1  0
PA		c1 c2 c3 c4
4	r1	7  8  9  +
5	r2	4  5  6  -
6	r3	1  2  3  *
7	r4	N  0  C  /
*/

// ex) scan KEY0 : PORTA |= (1<<2);  if(PINA & (1<<7))	
//     sacn KEY1 : PORTA |= (1<<3);  if(PINA & (1<<6))
// 중요생각 : 
int getkey(void){
	
	// testcode start
	/*
	PORTA = ~(1<<0);
	_delay_ms(20);
	if(!(PINA & (1<<7)))	return DIVIDE;
	if(!(PINA & (1<<4)))	return PLUS;
	if(!(PINA & (1<<5)))	return MINUS;
	if(!(PINA & (1<<6)))	return MULTIPLY;
	
	PORTA = ~(1<<1);
	_delay_ms(20);
	if(!(PINA & (1<<7)))	return CLEAR;
	if(!(PINA & (1<<4)))	return KEY9;
	if(!(PINA & (1<<5)))	return KEY6;
	if(!(PINA & (1<<6)))	return KEY3;

	
	PORTA = ~(1<<2);
	_delay_ms(20);
	if(!(PINA & (1<<7)))	return KEY0;
	if(!(PINA & (1<<4)))	return KEY8;
	if(!(PINA & (1<<5)))	return KEY5;
	if(!(PINA & (1<<6)))	return KEY2;

	
	PORTA = ~(1<<3);
	_delay_ms(20);
	if(!(PINA & (1<<7)))	return SPECIAL;
	if(!(PINA & (1<<4)))	return KEY7;
	if(!(PINA & (1<<5)))	return KEY4;
	if(!(PINA & (1<<6)))	return KEY1;

	*/
	// testcode end

	for(int i=0;i<4;i++){
		// column output select / row input internal-pullup
		PORTA = ~(1<<i);
		_delay_ms(20);
		for(int j=0;j<4;j++){
			// row input check
			if(!(PINA & ((1<<4) << j)))
				return (1<<4<<j) | (1<<i);
		}
	}

	return -1;
}
//// 4x4 keymat logic end

//// lcd init start

#define LCD_com_port		PORTE
#define RS		0
#define RW		1
#define EN		2

#define LCD_data_port		PORTF
#define LCD_com_port		PORTC

void LCD_com(unsigned char cmd){
	LCD_data_port = cmd;
	LCD_com_port &= ~(1<<RS);	// RS=0 command reg.
	LCD_com_port &= ~(1<<RW);	// RW=0 Write operation
	
	LCD_com_port |= (1<<EN);	// Enable pulse EN=1
	_delay_ms(10);
	LCD_com_port &= ~(1<<EN);	// EN=0
	_delay_ms(5);
}

void LCD_data(unsigned char c_data){
	LCD_data_port = c_data;
	LCD_com_port |= (1<<RS);	// RS=1 Data reg
	LCD_com_port &= ~(1<<RW);	// RW=0 write operation
	
	LCD_com_port |= (1<<EN);	// Enable Pulse
	_delay_ms(10);
	LCD_com_port &= ~(1<<EN);	// EN=0
	_delay_ms(5);
}

void LCD_init(void){
//	LCD_com_ddr = 0xFF;			// LCD command port out
//	LCD_data_ddr = 0xFF;		// LCD data port out

	_delay_ms(20);		// 15ms
	LCD_com(0x38);		// Initialization of 16X2 LCD in 8bit mode
	LCD_com(0x0C);		// Display ON Cursor OFF
	LCD_com(0x06);		// Auto increment cursor
	LCD_com(0x01);		// clear display
	LCD_com(0x80);		// cursor at home position
}

void LCD_char(unsigned char c){
	LCD_data(c);		// 문자하나를 출력
	_delay_ms(1);
}

void LCD_str(unsigned char *str){		// 문자열 출력
	while(*str != 0){					// 널문자(\0) 가 있을때가지 반복
		LCD_data(*str);					// 문자 하나를 출력
		str++;							// str의 다음문자 선택
	}
}

void LCD_string(unsigned char *str){
	int i;
	for(i=0;str[i]!=0;i++){		// 널문자(\0) 가 있을때까지 반복
		LCD_data(str[i]);		// data write
	}
}

void LCD_xy(unsigned char col, unsigned char row){
	LCD_com(0x80 | (row+col * 0x40));	// DDRAM (AC6~AC0) 0x40값
		// DDRAM 셋 = 0x80
		// row=1, 칸(가료) = 첫번쨰 칸
		// col=0, 줄(세로) = 첫번째 줄, col=1(두번째줄), 우선곱셈
		// 0x00=첫번째 줄 첫번지, 0x40=두번째 줄 첫번지
}

int prev_timer = 0;
void LCD_int(unsigned int n){
	for(int i=0;i<4;i++){
		LCD_data((n / disp_order[i] % 10) + 0x30);
	}
	prev_timer = n;
}

//// lcd init end

//// external interrupt init start
ISR(INT4_vect){
//	timer = 1;
	if(stat==DEFAULT){
		stat = SET_THOUSAND;
	}else if(stat == SET_THOUSAND){
		stat = DEFAULT;
		blink = 0;
	}else{
		// Do nothing
	}
}

ISR(INT5_vect){
//	timer = 10;
	if(stat==DEFAULT){
		stat = SET_HUNDRED;
	}else if(stat == SET_HUNDRED){
		stat = DEFAULT;
		blink = 0;
	}else{
		// Do nothing
	}
}

ISR(INT6_vect){
//	timer = 100;
	if(stat==DEFAULT){
		stat = SET_TEN;
	}else if(stat == SET_TEN){
		stat = DEFAULT;
		blink = 0;
	}
	else{
		// Do nothing
	}
}

ISR(INT7_vect){
//	timer = 1000;
	if(stat==DEFAULT){
		stat = SET_ONE;
	}else if(stat == SET_ONE){
		stat = DEFAULT;
		blink = 0;
	}else{
		// Do nothing
	}
}

void exti_init(){
	EIMSK = 0b11110000;		// INT4~INT7 인터럽트 인에이블
	EICRB = 0b10101010;		// INT4~INT7 하강 에지
//	SREG = 0x80;			// 전역 인터럽트 인에이블
//	sei();
}
//// external interrupt init end

int main(void)
{
	// fnd4 
	DDRD = 0xff;		// number
	DDRE = 0x0f;		// select : PE0 ~ PE3
	// external interrupt			 PE4 ~ PE7
	
	// lcd
	DDRC = 0xff;		// RS, RW, E
	DDRF = 0xff;		// DATA
		
	// keymat
	int key, prev_key, key_num;			// key --> switch-case --> key_num
	key = prev_key = -1;
	DDRA = 0x0f;		// row  : input,   column : output 
	PORTA = 0xf0;		// 입력핀의 풀업제어(상위4bit 를 풀업으로)
	
	timer0_init();
//	timer = 1234;
	timer2_init();

	LCD_init();
	LCD_com(0x00);
	LCD_string("Electronic SS");
	LCD_com(0xC0);
	LCD_string("seoul   seoul");
	
	exti_init();
    /* Replace with your application code */
    while (1) 
    {	
		if(prev_timer != timer){
			LCD_com(0x01);
			LCD_int(timer);		// prev_timer update
		}
		
		key = getkey();
		if((key != prev_key) && (key != -1)){
			switch(key){
				case KEY0:		key_num = 0;	break;
				case KEY1:		key_num = 1;	break;
				case KEY2:		key_num = 2;	break;
				case KEY3:		key_num = 3;	break;
				case KEY4:		key_num = 4;	break;
				case KEY5:		key_num = 5;	break;
				case KEY6:		key_num = 6;	break;
				case KEY7:		key_num = 7;	break;
				case KEY8:		key_num = 8;	break;
				case KEY9:		key_num = 9;	break;
				/*
				case PLUS:		key_num = 10;	break;
				case MINUS:		key_num = 11;	break;
				case MULTIPLY:	key_num = 12;	break;
				case DIVIDE:	key_num = 13;	break;	
				case CLEAR:		key_num = 14;	break;
				case SPECIAL:	key_num = 15;	break;				
				default:		key_num = 20;	break;
				*/
				default:		break;
			}
			
			// 	DEFAULT,  SET_THOUSAND,  SET_HUNDRED,  SET_TEN,  SET_ONE
			switch(stat){
				case SET_THOUSAND:
					if(key_num >= 6)		break;			// 0 ~ 5 까지만 가능
					
					timer -= ((timer/1000)%10) * 1000;
					timer += (key_num*1000);
					break;
				case SET_HUNDRED:
					timer -= ((timer/100)%10) * 100;
					timer += (key_num*100);
					break;
				case SET_TEN:
					if(key_num >= 6)		break;			// 0 ~ 5 까지만 가능
					
					timer -= ((timer/10)%10) * 10;
					timer += (key_num*10);	
					break;
				case SET_ONE:
					timer -= (timer)%10;
					timer += key_num;
					break;
				case DEFAULT:					break;
				default:						break;	
			}
			prev_key = key;
		}
//		_delay_ms(100);		// need to timer print
	}
}